package dictionary

import (
	"fmt"
	"io/ioutil"
	"strings"
	"sort"
	"math"
	"flag"
)

var m = map[string][]string{}
var datafile string

func init() {
	flag.StringVar(&datafile, "datafile", "data.tsv", "TSV data file for dictionary words")
	flag.Parse()

	dat, err := ioutil.ReadFile( datafile )
	check(err)

	lines := strings.Split(string(dat), "\r\n")

	for _, line := range lines {
		if ( line == "" ) {
			continue
		}
		tokens := strings.Split( line, "\t" )
		key := tokens[ 0 ]
		value := tokens[ 1 ]

		m[ key ] = append( m[ key ], value )
	}
	fmt.Println( "Finish init" )
}

func check( e error ) {
	if e != nil {
		panic(e)
	}
}

func dedup( s []string ) []string {
	var d []string

	for i, l := range s {
		if len(s) >= i + 2 && s[ i + 1 ] != l {
			d = append( d, l )
		} else if (len(s) == i+1) {
			d = append( d, l )
		}
	}

	return d
}

func difference( word string, match string ) int {
	diff := 0

	x := math.Min( float64( len( word ) ), float64( len( match ) ) )

	for i := 0; i < int( x ); i++ {
		if word[i] != match[i] {
			diff++
		}
	}

	return diff
}

func closest( word string, matches []string ) string {
	close := ""
	d := 1000

	for _, match := range matches {
		diff := difference( word, match )

		if diff < d {
			close = match
			d = diff
		}
	}

	return close
}

func Hash( word string ) string {
	// Force word to lowercase
	word = strings.ToLower( word )
	s := strings.Split( word, "" )
	sort.Strings( s )
	s = dedup( s )

	return strings.Join( s, "" )
}

func Word( word string ) string {
	a := Hash( word )
	matches, found := m[a]

	if found {
		fmt.Println( "Matches ", word, matches )

		return closest( word, matches )
	}

	return word;
}

func Phrase( phrase string ) string {
	corrected := ""

	for _, word := range strings.Split( phrase, " " ) {
		corrected += Word( word ) + " "
	}

	return corrected;
}
