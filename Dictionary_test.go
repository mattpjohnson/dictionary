package dictionary

import "testing"

func TestWord(t *testing.T) {

	cases := []struct {
		in, want string
	}{
		{"hello", "hello"},
		{"olleh", "hello"},
		{"olllllhe", "hello"},
		{"wrold", "world"},
		{"dlorw", "world"},
		{"bllue", "blue"},
	}

	for _, c := range cases {
		got := Word(c.in)
		if got != c.want {
			t.Errorf("Correct(%q) == %q, want %q", c.in, got, c.want)
		}
	}
}
